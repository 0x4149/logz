package logz

import (
	"fmt"
	"log"
	"os"
)

// colors
var (
	reset  = "\033[0m"
	red    = "\033[31m"
	green  = "\033[32m"
	yellow = "\033[33m"
	cyan   = "\033[36m"
	purple = "\033[35m"
)

var Info = func(s ...interface{}) {
	setLogFile()
	log.SetPrefix("INFO: ")

	var l string
	for _, x := range s {
		l += fmt.Sprintf("%v ", x)
	}

	if Verbos {
		fmt.Printf("%s%s%s %s \n", cyan, ":INFO:", reset, l)
	}

	log.Println(l)
}

var InInfo = func(s ...interface{}) {
	setLogFile()
	log.SetPrefix("INFO: ")

	var l string
	for _, x := range s {
		l += fmt.Sprintf("%v ", x)
	}

	if Verbos {
		fmt.Printf("%s%s%s %s", cyan, ":INFO:", reset, l)
	}

	log.Println(l)
}

var Debug = func(s ...interface{}) {
	setLogFile()
	log.SetPrefix("DEBUG: ")

	var l string
	for _, x := range s {
		l += fmt.Sprintf("%v ", x)
	}

	if Verbos {
		fmt.Printf("%s%s%s %s \n", green, ":DEBUG:", reset, l)
	}

	log.Println(l)
}

var Warn = func(s ...interface{}) {
	setLogFile()
	log.SetPrefix("WARNING: ")

	var l string
	for _, x := range s {
		l += fmt.Sprintf("%v ", x)
	}

	if Verbos {
		fmt.Printf("%s%s%s %s \n", yellow, ":WARNING:", reset, l)
	}

	log.Println(l)
}

var Error = func(s ...interface{}) {
	setLogFile()
	log.SetPrefix("ERROR: ")

	var l string
	for _, x := range s {
		l += fmt.Sprintf("%v ", x)
	}

	if Verbos {
		fmt.Printf("%s%s%s %s \n", red, ":ERROR:", reset, l)
	}

	log.Println(l)
}

var Fatal = func(s ...interface{}) {
	setLogFile()
	log.SetPrefix("FATAL: ")

	var l string
	for _, x := range s {
		l += fmt.Sprintf("%v ", x)
	}

	if Verbos {
		fmt.Printf("%s%s%s %s \n", purple, ":FATAL:", reset, l)
	}

	log.Println(l)
	os.Exit(1)
}
