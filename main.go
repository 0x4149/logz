package logz

import (
	"fmt"
	"io/fs"
	"log"
	"os"
	"time"
)

var (
	File     *os.File
	Location *time.Location
	Today    string
	Verbos   bool
)

func Run() {
	clearLogs()
	setLogFile()
}

func setLogFile() {
	if Location == nil {
		SetTimezone("UTC")
	}

	if t := time.Now().In(Location).Format("2006-01-02"); t != Today {
		Today = t
		clearLogs()
		createLogFile()
	}
}

// remove logs more than 3 days old
func clearLogs() {
	dir, err := os.Open("./log")
	if err != nil {
		fmt.Println(err)
		return
	}
	defer dir.Close()
	files, err := dir.ReadDir(0)
	if err != nil {
		fmt.Println(err)
		return
	}

	for _, f := range files {
		if f.IsDir() {
			continue
		}
		go func(f fs.DirEntry) {
			threshold := time.Now().Add(-3 * 24 * time.Hour)

			fileDate, err := time.Parse("2006-01-02.logz", f.Name())
			if err != nil {
				return
			}

			if fileDate.Before(threshold) {
				if err := os.Remove("./log/" + f.Name()); err != nil {
					fmt.Println(err)
				}
			}
		}(f)
	}
}

func VerbosMode() {
	Verbos = true
}

func SetTimezone(s string) {
	loc, err := time.LoadLocation(s)

	if err != nil {
		log.Fatal("unable to set timezone location.", err)
	}

	Location = loc
}

func createLogFile() {
	// create directory
	dir := "./log/"
	if _, err := os.Stat(dir); os.IsNotExist(err) {
		err = os.MkdirAll(dir, 0755)
		if err != nil {
			log.Fatal(err)
		}
	}

	// create file
	file, err := os.OpenFile(dir+Today+".logz", os.O_APPEND|os.O_CREATE|os.O_WRONLY, 0666)

	if err != nil {
		log.Fatal("unable to create log file.", err)
	}

	log.SetFlags(log.Ldate | log.Ltime | log.Lmsgprefix)
	log.SetOutput(file)
}
